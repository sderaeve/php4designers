<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title></title>
    </head>
    <body>
        <?php
        $saldo = 120;
        $bedrag = 20;
        
        $nieuwsaldo = $saldo + $bedrag;
        ?> 
        <h1>Storting</h1>
        <p> <?php echo $bedrag; ?> € wordt gestort op een rekening met een saldo
            van <?php echo $saldo?>. Het huidige saldo bedraagt nu <?php echo $nieuwsaldo?>.</p>
        
        <?php
        $saldo = $nieuwsaldo;
        $bedrag = 50;
        
        $nieuwsaldo = $saldo - $bedrag; 
        ?>
        
        <h1>Afhaling</h1>
        <p> <?php echo $bedrag; ?> € wordt afgehaald op een rekening met een saldo
            van <?php echo $saldo?>. Het huidige saldo bedraagt nu <?php echo $nieuwsaldo?>.</p>
    </body>
</html>
